Elixir Library for HDT
======================

This is a work-in-progress implementation of the `HDT
<http://www.rdfhdt.org/>`__ serialization of RDF data in the Elixir
programming language.
