# http://erlang.org/doc/apps/stdlib/io_protocol.html
defmodule Hdt.IO do

  @doc ~S"""
  Read the binary file till the end character is found.
  This differs from read_line in setting the terminating character.
  
  """  
  def read_till(file, char) do
    _read_till(file, char, <<>>)
  end

  # defp _read_till(file, end_char, :eof, acc) do
  #   {:ok, acc}
  # end

  defp _read_till(file, end_char, acc) do
    case IO.binread(file, 1) do
      :eof -> {:eof, acc}
      char when char == end_char -> {:ok, acc}
      char -> _read_till(file, end_char, acc <> char)
    end
  end

end
