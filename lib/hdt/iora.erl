%% http://erlang.org/doc/apps/stdlib/io_protocol.html

-module(iora).
-export([until_nullterm/3, get_nulltermline/1]).

until_nullterm(_ThisFar,eof,_MyStopCharacter) ->
    {done,eof,[]};
until_nullterm(ThisFar,CharList,MyStopCharacter) ->
    case
        lists:splitwith(fun(X) -> X =/= MyStopCharacter end,  CharList)
    of
  {L,[]} ->
            {more,ThisFar++L};
  {L2,[MyStopCharacter|Rest]} ->
      {done,ThisFar++L2++[MyStopCharacter],Rest}
    end.

get_nulltermline(IoServer) ->
    IoServer ! {io_request,
                self(),
                IoServer,
                {get_until, unicode, '', ?MODULE, until_nullterm, [$\0]}},
    receive
        {io_reply, IoServer, Data} ->
      Data
    end.