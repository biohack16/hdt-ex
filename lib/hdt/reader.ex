
defmodule Hdt.Reader do
  use Bitwise
    # SPECS
    # http://www.rdfhdt.org/hdt-binary-format/#vbyte
  # SPECS FROM JAVA CODE
  # http://nlp.stanford.edu/IR-book/html/htmledition/variable-byte-codes-1.html

  # [7:49]  
  # https://github.com/rdfhdt/hdt-java/blob/a1890753b967fded2b078fa712a233e51434f11a/hdt-java-core/src/main/java/org/rdfhdt/hdt/compact/integer/VByte.java#L62
  #  GitHub
  # rdfhdt/hdt-java
  # hdt-java - HDT Java library and tools. 
   
   

  # [7:49]  
  # https://github.com/rdfhdt/hdt-java/blob/a1890753b967fded2b078fa712a233e51434f11a/hdt-java-core/src/main/java/org/rdfhdt/hdt/compact/integer/VByte.java#L38
  #  GitHub
  # rdfhdt/hdt-java
  # hdt-java - HDT Java library and tools. 
   
   

  # [7:49]  
  # https://github.com/rdfhdt/hdt-java/blob/master/hdt-java-core/src/main/java/org/rdfhdt/hdt/compact/sequence/SequenceLog64.java
  #  GitHub
  # rdfhdt/hdt-java
  # hdt-java - HDT Java library and tools. 



    @doc ~S"""
    Read the HDT `file`

    ## Examples 

    iex> Hdt.Reader.load "test/data/swdf-2012-11-28.hdt.gz"
    {:ok, {:valid}}
    """
    def load(file) do
      {:ok, file} = File.open(file, [:read, :compressed])
      File.close(file)
    end

    @doc ~S"""
    Parse a VByte following HDT specs


    ## Examples

    iex> Hdt.Reader.read_vbyte file
    {:ok, value}
    """
    def read_vbyte(file) do
      _read_vbyte(file, 0, <<>>)
    end

    @doc ~S"""
      Typical implementation of Variable-Byte encoding for integers.
      http://nlp.stanford.edu/IR-book/html/htmledition/variable-byte-codes-1.html

      The first bit of each byte specifies whether there are more bytes available.
      Numbers from 0 to 126 are encoded using just one byte.
      Numbers from 127 to 16383 are encoded using two bytes.
      Numbers from 16384 to 2097151 are encoded using three bytes.
    """
    defp _read_vbyte(file, check_bit, acc) do
      value_127 = :binary.encode_unsigned(band(value, 127))
      case :binary.decode_unsigned(IO.binread(file, 1)) do
        value when band(value, 0x80)==0 -> _read_vbyte(file, acc <> value_127)
        value -> acc <> value_127)
      end
    end

  @doc ~S"""
  Parse the header of HDT


  ## Examples

      iex> :io.setopts(:standard_io, encoding: :latin1)
      iex> c("lib/hdt/iora.erl").
      iex> filename = "test/data/swdf-2012-11-28.hdt.gz"
      iex> {:ok, file} = File.open(filename, [:read, :compressed, :read_ahead])
      iex> _read_glocal_header(file)
      {}

  """
  def read_global_header(file) do
    << 0x24, 0x48, 0x44, 0x54, type::size(8), format::binary >> = :iora.get_nulltermline(file)
    {:ok, << properties::binary >>} = Hdt.IO.read_till(file, "\0")
    << crc16::size(16) >> = IO.binread(file, 2)
    %{type: type, format: format, properties: properties, crc16: crc16}
  end

  def read_header(file) do
    << 0x24, 0x48, 0x44, 0x54, type::size(8), format::binary >> = :iora.get_nulltermline(file)
    {:ok, << properties::binary >>} = Hdt.IO.read_till(file, "\0")
    [<<"length=", length::binary>>, "" ] = String.split(properties,";")
    {length, _} = Integer.parse(length)
    << crc16::size(16) >> = IO.binread(file, 2)
    ntriples = IO.binread(file, length) 
    %{type: type,
    format: format,
    properties: properties,
    length: length,
    ntriples: ntriples,
    crc16: crc16 }
  end

  def read_dictionary(file) do
    << 0x24, 0x48, 0x44, 0x54, type::size(8), format::binary >> = :iora.get_nulltermline(file)
    << properties::binary >> = :iora.get_nulltermline(file)
    << crc16::size(16) >> = IO.binread(file, 2)
    [<<"mapping=", mapping::binary>>, <<"sizeStrings=", sizeStrings::binary>>, <<0>>] = String.split(properties,";")
    {mapping, _} = Integer.parse(mapping)
    {sizeStrings, _} = Integer.parse(sizeStrings)
    << dictionary_type::size(8) >>  = IO.binread(file, 1)
    total_number_of_strings_stored = Hdt.Reader.read_vbyte(file) # need to be tested
    length_of_the_buffer_containing_the_packed_strings = Hdt.Reader.read_vbyte(file) # need to be tested
    << crc8::size(8) >> = IO.binread(file,1) # not sure that is goes here.
    %{type: type, format: format,
    properties: properties, 
    mapping: mapping, 
    sizeStrings: sizeStrings,
    dictionary_type: dictionary_type,
    total_number_of_strings_stored: total_number_of_strings_stored,
    length_of_the_buffer_containing_the_packed_strings: length_of_the_buffer_containing_the_packed_strings,
    crc8: crc8}
  end
end


  # def parse_header(file) do
  #   #http://stackoverflow.com/questions/34458855/why-do-file-reads-differ-depending-on-stdio-or-file-open
  #   #http://erlang.org/pipermail/erlang-bugs/2014-July/004498.html
    :io.setopts(:standard_io, encoding: :latin1)
  #   c "lib/hdt/iora.erl"
    filename = "test/data/swdf-2012-11-28.hdt.gz"
    {:ok, file} = File.open(filename, [:read, :compressed, :read_ahead])
    
    # Global Header
    global_header = Hdt.Reader.read_global_header(file) 
    # Header
    header = Hdt.Reader.read_header(file)
    # Dictionary
    dictionary = Hdt.Reader.read_dictionary(file)

    File.close(file)

    # IO.puts global_header
    # IO.puts header
    # IO.puts dictionary
  #   [format | data ] = String.split(data, "\0", parts: 2)
  #   [properties_data | data ] = data
  #   properties= String.split(properties_data, ";")
  #   IO.puts(x)
  #   x
    
    
  # end
