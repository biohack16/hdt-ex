defmodule CliTest do
  use ExUnit.Case
  doctest Hdt

  import Hdt.CLI, only: [parse_args: 1]

  test ":help returned if -h or --help given" do
    assert parse_args(["-h"]) == :help
    assert parse_args(["-h", "something"]) == :help
  end
end
